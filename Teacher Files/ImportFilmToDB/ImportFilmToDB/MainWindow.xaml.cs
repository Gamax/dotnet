﻿using FilmDBManagement;
using FilmDTOLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImportFilmToDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // <copyright file="MainWindow" company="Haute Ecole de la Province de Liège">
            // Copyright (c) 2016 All Rights Reserved
            // <author>Cécile Moitroux</author>
            // </copyright>
        private StreamReader f = null;
        private FilmDBManager _dbman = null;
        private string line;

        public MainWindow()
        {
            InitializeComponent();            
        }

        private void importWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Open file to read it
            f = new StreamReader(@"E:\Thomas\git\dotnet\movies.txt");            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Close file
            f.Close();
        }

        private void databaseopen_Click(object sender, RoutedEventArgs e)
        {
            _dbman = FilmDBManager.Singleton(Servername.Text, DBname.Text);
            if (_dbman != null)
            {
                readline.IsEnabled = true;
                openDB.IsEnabled = false;
                Servername.IsEnabled = false;
                DBname.IsEnabled = false;
            }
        }

        private void readline_Click(object sender, RoutedEventArgs e)
        {
            readAnddecodeline();
        }

        private bool readAnddecodeline()
        {
            line = f.ReadLine();
            if (line == null)
                return false;
            filmfulldata.Text = line;

            // Creation d'un objet film
            FilmText filmtext = new FilmText();
            FilmDTO film = filmtext.DecodeFilmText(line);

            // Mise à jour de l'interface utilisateur
            AfficheFilm(filmtext, film);

            // Enregistrement des données du film dans la base de données uniquement si la longueur du titre est < à 100 char
            if (film.Title.Length <= 100)
                SaveFilmToDataBase(film);

            return true;
        }

        private void SaveFilmToDataBase(FilmDTO film)
        {
            foreach (GenreDTO g in film.Genrelist)                 _dbman.AddGenre(g);
            foreach (RealisateurDTO r in film.Realisateurlist)     _dbman.AddRealisateur(r);
            foreach (ActeurDTO a in film.Acteurlist)               _dbman.AddActeur(a);
            _dbman.AddFilm(film);
            foreach (GenreDTO g in film.Genrelist)              _dbman.AddGenreRelation(film,g);
            foreach (RealisateurDTO r in film.Realisateurlist)  _dbman.AddRealisateurRelation(film,r);
            foreach (ActeurDTO a in film.Acteurlist)            _dbman.AddActeurRelation(film,a);
        }

        private void AfficheFilm(FilmText filmtext, FilmDTO film)
        {
            // Affichage de la ligne lue dans la textbox
            filmdetaildata.Text = "";
            foreach (string s in filmtext.Filmdetailwords)
                filmdetaildata.Text += s + "\n";
            // Affichage des Genres décodés
            filmdetaildata.Text += "GENRES\n";
            foreach (GenreDTO g in film.Genrelist)
            {
                filmdetaildata.Text += "\t" + g.Id + "\t" + g.Name + "\n";
            }
            // Affichage des Réalisateurs décodés
            filmdetaildata.Text += "REALISATEURS\n";
            foreach (RealisateurDTO r in film.Realisateurlist)
            {
                filmdetaildata.Text += "\t" + r.Id + "\t" + r.Name + "\n";
            }
            // Affichage des Acteurs décodés
            filmdetaildata.Text += "ACTEURS\n";
            foreach (ActeurDTO a in film.Acteurlist)
            {
                filmdetaildata.Text += "\t" + a.Id + "\t" + a.Name + "\t" + a.Character + "\n";
            }
        }

        private void CountLine_Click(object sender, RoutedEventArgs e)
        {
            f.Close();
            f = new StreamReader(@"C:\Data\OneDrive\HEPL 2016-2017\B2 3 INFO SYST GEST Prog .NET\Lecture DB\ImportFilmToDB\movies.txt");
            int i = 0;
            do
            {
                line = f.ReadLine();
                if (i % 100 == 0)
                    i++;
                filmfulldata.Text = i.ToString();
            }
            while (line != null);
            filmfulldata.Text = i.ToString();

            f.Close();
            f = new StreamReader(@"C:\Data\OneDrive\HEPL 2016-2017\B2 3 INFO SYST GEST Prog .NET\Lecture DB\ImportFilmToDB\movies.txt");

        }
    }
}
