﻿using FilmDTOLibrary;
using FimDBManagement;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FilmDBManagement
{
    public class FilmDBManager
    {
        // <copyright file="FilmDBManager" company="Haute Ecole de la Province de Liège">
            // Copyright (c) 2016 All Rights Reserved
            // <author>Cécile Moitroux</author>
            // </copyright>
        FilmDBDataClassesDataContext context = null;
        
        private static FilmDBManager _instance;

        public static FilmDBManager Singleton(String servername, String dbname)
        {
            return _instance ?? (_instance = new FilmDBManager(servername,dbname));
        }
        private FilmDBManager(String servername, String dbname)
        {
            if (dbname == null || dbname == "")
                context = new FilmDBDataClassesDataContext();
            else
            {
                String connectionstring = "Data Source = " + servername + " ; Initial Catalog =" + dbname + "; Integrated Security = True";
                context = new FilmDBDataClassesDataContext (connectionstring);
                if (!context.DatabaseExists())  // Vérifier si la DB existe
                    context.CreateDatabase();   // Si elle n'existe pas, on la crée
            }
        }

        public bool Add<T>(T rec, Func<T,bool> expr) where T : class
        {
            if (context == null)
                throw new Exception("DAL not connected");
            try
            {
                // Query qui permet d'accéder à l'ensemble des objets d'une table dont le type es passé en paramètre
                IQueryable<T> query = ((Table<T>)context.GetType().GetProperty(typeof(T).Name + "s").GetValue(context));
                if(!query.Where(expr).Any()) // Vérifie sur base de l'expression que aucun objet ne correspond au critère de recherche
                {
                    ((Table<T>)context.GetType().GetProperty(typeof(T).Name + "s").GetValue(context)).InsertOnSubmit(rec);
                    context.SubmitChanges();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Add<T,String,Q> (T rec, string sid) where T : class
        {
            if (context == null)
                throw new Exception("DAL not connected");
            try
            {
                var valeur = rec.GetType().GetProperty(sid).GetValue(rec);
                IQueryable<T> query = ((Table<T>)context.GetType().GetProperty(typeof(T).Name + "s").GetValue(context));
                query = Simplified<T,Q>(query, sid, (Q)valeur);
                if (query.Count() == 0)
                {
                    ((Table<T>)context.GetType().GetProperty(typeof(T).Name + "s").GetValue(context)).InsertOnSubmit(rec);
                    context.SubmitChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        static IQueryable<T> Simplified<T,Q>(IQueryable<T> query, string propertyName, Q propertyValue)
        {
            PropertyInfo propertyInfo = typeof(T).GetProperty(propertyName);
            return Simplified<T,Q>(query, propertyInfo, propertyValue);
        }

        static IQueryable<T> Simplified<T,Q>(IQueryable<T> query, PropertyInfo propertyInfo, Q propertyValue)
        {
            ParameterExpression e = Expression.Parameter(typeof(T), "e");
            MemberExpression m = Expression.MakeMemberAccess(e, propertyInfo);
            ConstantExpression c = Expression.Constant(propertyValue, propertyValue.GetType());
            BinaryExpression b = Expression.Equal(m, c);

            Expression<Func<T, bool>> lambda = Expression.Lambda<Func<T, bool>>(b, e);
            return query.Where(lambda);
        }


        #region GENRES
        public bool AddGenre(GenreDTO g)
        {
            Genre newg = new Genre
            {
                id = g.Id,
                name = g.Name
            };
            //return Add<Genre,String,int>(newg,"id");
            return Add<Genre>(newg, xg => xg.id == g.Id);
        }

        public void AddGenreRelation(FilmDTO film, GenreDTO g)
        {
            FilmGenre newfg = new FilmGenre
            {
                id = 0,
                id_film = film.Id,
                id_genre = g.Id
            };
            Add<FilmGenre>(newfg, xg => xg.id == g.Id);
        }

        public bool AddGenre(int _id, string _name)
        {
            // On vérifie si le genre n'a pas déjà été enregistré ds la BD
            var gen = from d in context.Genres
                      where _id == d.id
                      select d;
            if (gen.Count() == 0)
            {
                Genre g = new Genre
                {
                    id = _id,
                    name = _name
                };

                // Add the new object to the Orders collection.
                context.Genres.InsertOnSubmit(g);

                // Submit the change to the database.
                try
                {
                    context.SubmitChanges();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Make some adjustments.
                    // ...
                    // Try again.
                    //context.SubmitChanges();
                    return false;
                }
            }
            else
                return true;
        } // non utilisé
        #endregion

        #region REALISATEUR
        public void AddRealisateurRelation(FilmDTO film, RealisateurDTO r)
        {
            FilmRealisateur newfr = new FilmRealisateur
            {
                id = 0,
                id_film = film.Id,
                id_realisateur= r.Id
            };
            Add<FilmRealisateur>(newfr, xg => xg.id == r.Id);
        }

        public bool AddRealisateur(RealisateurDTO r)
        {
            Realisateur newr = new Realisateur
            {
                id = r.Id,
                name = r.Name
            };
            return Add<Realisateur>(newr, xg => xg.id == r.Id);
        }

        public bool AddRealisateur(int _id, string _name) //non utilisé
        {
            // On vérifie si le genre n'a pas déjà été enregistré ds la BD
            var rea = from d in context.Realisateurs
                      where _id == d.id
                      select d;
            if (rea.Count() == 0)
            {
                Realisateur g = new Realisateur
                {
                    id = _id,
                    name = _name
                };

                // Add the new object to the Orders collection.
                context.Realisateurs.InsertOnSubmit(g);

                // Submit the change to the database.
                try
                {
                    context.SubmitChanges();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Make some adjustments.
                    // ...
                    // Try again.
                    //context.SubmitChanges();
                    return false;
                }
            }
            else
                return true;
        }
        #endregion

        #region ACTEUR
        public bool AddActeur(ActeurDTO a)
        {
            Actor newa = new Actor
            {
                id = a.Id,
                name = a.Name,
                character = a.Character
            };
            return Add<Actor>(newa, xg => xg.id == a.Id);
        }

        public void AddActeurRelation(FilmDTO film, ActeurDTO a)
        {
            FilmActor newfa = new FilmActor
            {
                id = 0,
                id_film = film.Id,
                id_actor = a.Id
            };
            Add<FilmActor>(newfa, xg => xg.id == a.Id);
        }       

        public bool AddActeur(int _id, string _name, string _character)
        {
            // On vérifie si l'acteur et son personnage n'a pas déjà été enregistré ds la BD
            var act = from d in context.Actors
                      where _id == d.id
                      select d;
            if (act.Count() == 0)
            {
                Actor g = new Actor
                {
                    id = _id,
                    name = _name,
                    character = _character
                };

                // Add the new object to the Orders collection.
                context.Actors.InsertOnSubmit(g);

                // Submit the change to the database.
                try
                {
                    context.SubmitChanges();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Make some adjustments.
                    // ...
                    // Try again.
                    //context.SubmitChanges();
                    return false;
                }
            }
            else
                return true;
        } //non utilisé

        #endregion

        #region FILM
        public bool AddFilm(FilmDTO f)
        {
            Film newf = new Film
            {
                id = f.Id,
                title = f.Title,
                original_title = f.Original_title,
                runtime = f.Runtime,
                posterpath = f.Posterpath
            };
            return Add<Film>(newf, xg => xg.id == newf.id);
        }

        public bool AddFilm(int _id, string _title, string _originaltitle, int _runtime, string _posterpath)
        {
            // On vérifie si le film n'a pas déjà été enregistré ds la BD
            var films = from f in context.Films
                      where _id == f.id
                      select f;
            if (films.Count() == 0)
            {
                Film f = new Film
                {
                    id = _id,
                    title = _title,
                    original_title = _originaltitle,
                    runtime = _runtime,
                    posterpath = _posterpath
                };

                // Add the new object to the Orders collection.
                context.Films.InsertOnSubmit(f);

                // Submit the change to the database.
                try
                {
                    context.SubmitChanges();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Make some adjustments.
                    // ...
                    // Try again.
                    //context.SubmitChanges();
                    return false;
                }
            }
            else
                return true;     
        }
        #endregion
    }
}
