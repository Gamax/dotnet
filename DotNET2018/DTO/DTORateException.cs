﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTORateException : Exception
    {
        public DTORateException () : base () 
        {

        }

        public DTORateException(string str) : base(str)
        {

        }
    }
}
