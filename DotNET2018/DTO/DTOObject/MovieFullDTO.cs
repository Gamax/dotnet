﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "MovieFullDTO")]
    public class MovieFullDTO : MovieDTO
    {
        #region variables
        private List<MovieTypeDTO> _movieTypes;
        private List<ActorDTO> _actors;
        #endregion

        #region proprietes
        [DataMember(Name = "MovieType")]
        public virtual List<MovieTypeDTO> MovieTypes
        {
            get { return _movieTypes; }
            set { _movieTypes = value; }
        }
        [DataMember(Name = "MovieActors")]
        public virtual List<ActorDTO> Actors
        {
            get { return _actors; }
            set { _actors = value; }
        }

        #endregion

        public MovieFullDTO(int iD, string title, int runtime, string posterPath, List<MovieTypeDTO> movieTypes, List<ActorDTO> actors) : base (iD,title,runtime,posterPath)
        {
            _movieTypes = movieTypes;
            _actors = actors;
        }

        public MovieFullDTO() : base()
        {

        }

        public override string ToString()
        {

            StringBuilder s = new StringBuilder();
            s.Append(base.ToString() + "\n");

            foreach(ActorDTO actor in Actors)
            {
                s.Append(actor.ToString() + "\n");
            }

            foreach(MovieTypeDTO genre in MovieTypes)
            {
                s.Append(genre.ToString() + "\n");
            }

            return s.ToString();
        }

    }
}
