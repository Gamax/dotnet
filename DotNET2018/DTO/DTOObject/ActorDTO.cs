﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "ActorDTO")]
    public class ActorDTO
    {
        #region variables
        private int _id;
        private string _name;


        #endregion

        #region proprietes
        [DataMember(Name = "ActorID")]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember(Name = "ActorName")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        public ActorDTO(int iD, string name)
        {
            _id = iD;
            _name = name;
        }

        public ActorDTO()
        {

        }

        public override string ToString()
        {
            return "(" + ID + "," + Name + ")";
        }
    }
}
