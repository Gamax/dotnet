﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "LightActorDTO")]
    public class LightActorDTO
    {
        #region variables
        private string _name;
        private int _nbMovie;


        #endregion

        #region proprietes
        [DataMember(Name = "LightActorName")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember(Name = "LightActorNBFilm")]
        public int NbMovie { get => _nbMovie; set => _nbMovie = value; }

        

        #endregion

        public LightActorDTO(string name, int nbMovie)
        {
            _name = name;
            NbMovie = nbMovie;
        }
        public LightActorDTO()
        {

        }

        public override string ToString()
        {
            return "("+Name+","+NbMovie+")";
        }
    }
}
