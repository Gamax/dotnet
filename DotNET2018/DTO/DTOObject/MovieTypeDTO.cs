﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "MovieTypeDTO")]
    public class MovieTypeDTO
    {
        #region variables
        private int _id;
        private string _name;


        #endregion

        #region proprietes
        [DataMember(Name = "MovieTypeID")]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember(Name = "MovieTypeName")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        public MovieTypeDTO(int iD, string name)
        {
            _id = iD;
            _name = name;
        }

        public MovieTypeDTO()
        {

        }

        public override string ToString()
        {
            return "("+ID+","+Name+")";
        }
    }
}
