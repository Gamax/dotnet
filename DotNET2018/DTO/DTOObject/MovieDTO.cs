﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "MovieDTO")]
    public class MovieDTO
    {

        #region variables
        private int _id;
        private string _title;
        private int _runtime;
        private string _posterpath;


        #endregion

        #region proprietes
        [DataMember(Name = "MovieID")]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember(Name = "MovieTitle")]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        [DataMember(Name = "MovieRuntime")]
        public int Runtime
        {
            get { return _runtime; }
            set { _runtime = value; }
        }
        [DataMember(Name = "MoviePoster")]
        public string PosterPath
        {
            get { return _posterpath; }
            set { _posterpath = value; }
        }

        #endregion

        public MovieDTO(int iD, string title, int runtime, string posterPath)
        {
            _id = iD;
            _title = title;
            _runtime = runtime;
            _posterpath = posterPath;
        }
        public MovieDTO()
        {

        }

        public override string ToString()
        {
            return "("+ID+","+Title+","+Runtime+","+PosterPath+")";
        }
    }
}
