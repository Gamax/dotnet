﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [DataContract(Name = "CommentDTO")]
    public class CommentDTO
    {
        #region variables
        private string _content;
        private int _rate;
        private int _idfilm;
        private string _avatar;
        private DateTime _date;


        #endregion

        #region proprietes
        [DataMember(Name = "CommentContent")]
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        [DataMember(Name = "CommentRate")]
        public int Rate
        {
            get { return _rate; }
            set {
                if (value < 0) throw new DTORateException("note trop basse !");
                if (value > 5) throw new DTORateException("note trop haute !");
                _rate = value;
            }
        }
        [DataMember(Name = "CommentIDFilm")]
        public int IDFilm
        {
            get { return _idfilm; }
            set { _idfilm = value; }
        }
        [DataMember(Name = "CommentAvatar")]
        public string Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }
        [DataMember(Name = "CommentDate")]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        #endregion

        #region constructor
        public CommentDTO(string content, int rate, int iDFilm, string avatar, DateTime date)
        {
            Content = content;
            Rate = rate;
            IDFilm = iDFilm;
            Avatar = avatar;
            Date = date;
        }

        public CommentDTO(string content, int rate, int iDFilm, string avatar)
        {
            Content = content;
            Rate = rate;
            IDFilm = iDFilm;
            Avatar = avatar;
        }

        public CommentDTO()
        {

        }
        #endregion

        public override string ToString()
        {
            return "("+IDFilm+","+Avatar+","+ Rate + ","+Content+")";
        }

    }
}
