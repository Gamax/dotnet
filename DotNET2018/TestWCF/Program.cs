﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using TestWCF.ServiceRef;
using DTO;

namespace TestWCF
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = "";

            try
            {
                var host = new ServiceRef.WCFServiceClient();
                Console.WriteLine("Lancement du service");
                host.Open();

                Console.WriteLine("Entrer un id de film pour lister les acteurs : ");
                input = Console.ReadLine();
                Console.WriteLine($"Liste des acteurs de {input}");
                ActorDTO[] actorsDTO = host.GetListActorsByIdFilm(Int32.Parse(input));

                foreach (ActorDTO a in actorsDTO)
                {
                    Console.WriteLine(a.ToString()); ;
                }

                Console.WriteLine("Entrer un id de film pour lister les genres : ");
                input = Console.ReadLine();
                Console.WriteLine($"Liste des genres de {input}");
                MovieTypeDTO[] movieTypeDTOs = host.GetMovieTypeListByMovieId(Int32.Parse(input));

                foreach (MovieTypeDTO m in movieTypeDTOs)
                {
                    Console.WriteLine(m.ToString());
                }

                Console.WriteLine("Entrer un nom d'acteur partiel pour lister les films : ");
                input = Console.ReadLine();
                Console.WriteLine($"Liste des films de {input}");
                MovieDTO[] movieDTOs = host.FindMovieListByPartialActorName(input);

                foreach (MovieDTO movie in movieDTOs )
                {
                    Console.WriteLine(movie.ToString());
                }

                Console.WriteLine("Acteurs favoris : ");
                LightActorDTO[] lightActorDTOs = host.GetFavoriteActors();

                foreach (LightActorDTO actor in lightActorDTOs)
                {
                    Console.WriteLine(actor.ToString());
                }

                Console.WriteLine("Entrer un id de film pour en voir les détails : ");
                input = Console.ReadLine();
                MovieFullDTO movieFullDTO = host.GetMovieFullDetailsByMovieId(Int32.Parse(input));

                Console.WriteLine(movieFullDTO.ToString());

                Console.Out.WriteLine("----- InsertCommentOnMovieId -----");
                Console.Out.Write("Content : ");
                String content = Console.In.ReadLine();
                Console.Out.Write("Rate : ");
                int rate = Int32.Parse(Console.In.ReadLine());
                Console.Out.Write("Avatar : ");
                String avatar = Console.In.ReadLine();
                Console.Out.Write("IDFilm : ");
                int IdFilm = Int32.Parse(Console.In.ReadLine());

                try
                {
                    CommentDTO commentDTO = new CommentDTO(content, rate, IdFilm, avatar);
                    Console.Out.WriteLine("----- SendingComment -----");

                    host.InsertCommentOnMovieId(commentDTO);

                    Console.Out.WriteLine("----- Comment Sent ! -----");
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(e.ToString());
                    Console.ReadKey();
                    return;

                }


                Console.WriteLine("Fermeture du service...");
                host.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur lors du lancement du service :" + ex.Message);
            }

            Console.ReadKey();
        }
    }
}
