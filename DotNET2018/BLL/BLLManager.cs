﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;


namespace BLL
{
    public class BLLManager
    {

        DALManager _dALManager;

        public BLLManager()
        {
            _dALManager = new DALManager();
        }

        #region EtapeBLL
        public List<ActorDTO> GetListActorsByMovieId(int idMovie)
        {
            IQueryable<Movie> movies = _dALManager.GetMovies();

            var query = movies.Where(m => m.ID == idMovie);


            Movie movie;
            try
            {
                movie = query.Single<Movie>();
            }
            catch(InvalidOperationException)
            {
                return new List<ActorDTO>();
            }
            


            List<Actor> listActor = movie.Actors;

            List<ActorDTO> listActorDTO = new List<ActorDTO>();

            foreach(Actor actor in listActor)
            {
                listActorDTO.Add(new ActorDTO(actor.ID, actor.Name));
            }

            return listActorDTO;
        }

        public List<MovieTypeDTO> GetMovieTypeListByMovieId(int idMovie)
        {

            IQueryable<Movie> movies = _dALManager.GetMovies();

            var query = movies.Where(m => m.ID == idMovie);


            Movie movie;
            try
            {
                movie = query.Single<Movie>();
            }
            catch (InvalidOperationException e)
            {
                return new List<MovieTypeDTO>();
            }



            List<Genre> listgenre = movie.Genres;

            List<MovieTypeDTO> listgenreDTO = new List<MovieTypeDTO>();

            foreach (Genre genre in listgenre)
            {
                listgenreDTO.Add(new MovieTypeDTO(genre.ID, genre.Name));
            }

            return listgenreDTO;
        }

        public List<MovieDTO> FindMovieListByPartialActorName(String actorName)
        {

            IQueryable<Movie> movies = _dALManager.GetMovies();

            IQueryable<Actor> actors = _dALManager.GetActors();


            var query = actors.Where<Actor>(a => a.Name.Contains(actorName)).Select(a => a.Movies);

            

            List < MovieDTO > listMovie = new List<MovieDTO>();
            foreach (List<Movie> moviesl in query)
            {

                foreach (Movie movie in moviesl)
                {
                    if(!listMovie.Exists(m => m.ID == movie.ID))
                        listMovie.Add(new MovieDTO(movie.ID, movie.Title, movie.Runtime, movie.PosterPath));
                }
            }
            

            return listMovie;
        }

        public List<LightActorDTO> GetFavoriteActors() 
        {
            IQueryable<Actor> actors = _dALManager.GetActors();

            IQueryable<Actor> query = actors.Where<Actor>(a => a.Movies.Count > 1);

            List<LightActorDTO> listActor = new List<LightActorDTO>();

            foreach(Actor actor in query)
            {
                listActor.Add(new LightActorDTO(actor.Name, actor.Movies.Count));
            }

            return listActor;
        }

        public MovieFullDTO GetMovieFullDetailsByMovieId(int idMovie)
        {

            //import dal
            IQueryable < Movie > movies = _dALManager.GetMovies();

            //Recup movie

            IQueryable<Movie> queryMovie = movies.Where<Movie>(m => m.ID == idMovie);

            Movie movie = queryMovie.First<Movie>();

            //Recup Acteur

            List<ActorDTO> actorsInMovie = new List<ActorDTO>();

            foreach (Actor actor in movie.Actors)
            {
                actorsInMovie.Add(new ActorDTO(actor.ID, actor.Name));
            }

            //Recup Genre

            List<MovieTypeDTO> genresInMovie = new List<MovieTypeDTO>();

            foreach (Genre genre in movie.Genres)
            {
                genresInMovie.Add(new MovieTypeDTO(genre.ID, genre.Name));
            }


            return new MovieFullDTO(movie.ID,movie.Title,movie.Runtime,movie.PosterPath,genresInMovie,actorsInMovie);
        }

        public void InsertCommentOnMovieId(CommentDTO comment)
        {
            _dALManager.AddComment(comment.Content, comment.Rate, comment.IDFilm, comment.Avatar,DateTime.Now);
        }
        #endregion

        #region ASPNET
        public List<MovieDTO> getMoviesFromXToY(int debut, int combien)
        {
            IQueryable<Movie> movies = _dALManager.GetMoviesFromXToY(debut,combien);

            List<MovieDTO> listfilm = new List<MovieDTO>();
            foreach(Movie film in movies)
            {
                listfilm.Add(new MovieDTO(film.ID,film.Title,film.Runtime,film.PosterPath));
            }
            return listfilm;
        }

        public List<CommentDTO> getCommentsFor(int idMovie)
        {
            IQueryable<Comment> comments = _dALManager.GetCommentsFor(idMovie);

            List<CommentDTO> commentaires = new List<CommentDTO>();

            foreach(Comment comment in comments)
            {
                commentaires.Add(new CommentDTO(comment.Content, comment.Rate, comment.IDFilm, comment.Avatar,comment.Date));
            }
            return commentaires;
        }

        public double getAverageFor(int idMovie)
        {
            IQueryable<Comment> comments = _dALManager.GetCommentsFor(idMovie);

            int i = 0;
            double rate = 0;

            foreach (Comment comment in comments)
            {
                i++;
                rate = rate + comment.Rate;
            }
            if (i == 0)
                return 0;

            return rate/i;
        }
        #endregion
    }
}
