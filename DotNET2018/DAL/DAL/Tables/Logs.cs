﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Logs
    {
        public static string LOG_ERREUR = "-1";
        public static string LOG_INFO = "1";
        public static string LOG_DEFAULT = "0";

        private DateTime _date;
        private string _table;
        private int _idTuple;
        private string _code;
        private string _message;

        public Logs()
        {
            Code = LOG_DEFAULT;
            Date = new DateTime();
        }

        public Logs(string code, string table, int idTuple, string message)
        {
            Code = code;
            Table = table;
            IdTuple = IdTuple;
            Message = message;
        }



        public string Code { get => _code; set => _code = value; }
        public string Table { get => _table; set => _table = value; }
        public int IdTuple { get => _idTuple; set => _idTuple = value; }
        public string Message { get => _message; set => _message = value; }
        public DateTime Date { get => _date; set => _date = value; }

        public override string ToString()
        {
            return Date + "_" + Table + "_" + IdTuple + " : " + " " + Code + " : " + Message;
        }
    }
}
