﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Movie : TableSQL
    {
        #region variables
        private int _id;
        private string _title;
        private int _runtime;
        private string _posterpath;
        private List<Genre> _genres;
        private List<Actor> _actors;
        #endregion

        #region proprietes
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public int Runtime
        {
            get { return _runtime; }
            set { _runtime = value; }
        }

        public string PosterPath
        {
            get { return _posterpath; }
            set { _posterpath = value; }
        }

        public virtual List<Genre> Genres
        {
            get { return _genres; }
            set { _genres = value; }
        }

        public virtual List<Actor> Actors
        {
            get { return _actors; }
            set { _actors = value; }
        }

        #endregion

        #region constructeurs
        public Movie()
        {
            Genres = new List<Genre>();
            Actors = new List<Actor>();
        }

        public Movie(int id, string title, int runtime, string posterpath, List<Genre> genres, List<Actor> actors)
        {
            ID = id;
            Title = title;
            Runtime = runtime;
            PosterPath = posterpath;
            Genres = genres;
            Actors = actors;
        }

        public Movie(Movie original)
        {
            ID = original.ID;
            Title = original.Title;
            Runtime = original.Runtime;
            PosterPath = original.PosterPath;
            Genres = original.Genres;
            Actors = original.Actors;
        }
        #endregion

        #region other methods
        public void Affiche()
        {
            Console.WriteLine("ID : " + ID);
            Console.WriteLine("Title : " + Title);
            Console.WriteLine("Runtime : " + Runtime);
            Console.WriteLine("PosterPath : " + PosterPath);
            Console.WriteLine("Genres : ");
            foreach (Genre g in Genres)
            {
                Console.WriteLine(" -" + g.ToString());
            }
            Console.WriteLine("Actors : ");
            foreach (Actor a in Actors)
            {
                Console.WriteLine(" -" + a.ToString());
            }
        }

        public Logs SetLog(string erreur)
        {
            Logs log = new Logs
            {
                Table = "Movie",
                IdTuple = ID,
                Message = erreur
            };

            return log;
        }
        #endregion

    }
}
