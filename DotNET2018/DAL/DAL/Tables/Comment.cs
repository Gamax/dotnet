﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Comment : TableSQL
    {
        #region variables
        private int _id;
        private string _content;
        private int _rate;
        private int _idfilm;
        private string _avatar; //nom user
        private DateTime _date;

        #endregion

        #region proprietes
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        public int Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }
        public int IDFilm
        {
            get { return _idfilm; }
            set { _idfilm = value; }
        }
        public string Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        #endregion

        #region constructeurs
        public Comment()
        {

        }

        public Comment( string content, int rate, int iDFilm, string avatar, DateTime date)
        {
            Content = content;
            Rate = rate;
            IDFilm = iDFilm;
            Avatar = avatar;
            Date = date;
        }

        #endregion

        #region other methods
        public Logs SetLog(string erreur)
        {
            Logs log = new Logs();
            log.Table = "Comment";
            log.IdTuple = ID;
            log.Message = erreur;

            return log;
        }
        #endregion
    }
}
