﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Genre : TableSQL
    {
        #region variables
        private int _id;
        private string _name;
        private List<Movie> _movies; 
        #endregion

        #region proprietes
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual List<Movie> Movies { get => _movies; set => _movies = value; }

        #endregion

        #region constructeurs
        public Genre()
        {

        }

        public Genre(string name)
        {
            Name = name;
        }
        #endregion
        public override string ToString()
        {
            return ID + " : " + Name;
        }

        public Logs SetLog(string erreur)
        {
            Logs log = new Logs();
            log.Table = "Genre";
            log.IdTuple = ID;
            log.Message = erreur;

            return log;
        }
        #region other methods

        #endregion
    }
}
