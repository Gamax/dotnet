﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALManager
    {

        private ContextDotNET2018 _dbContext;

        public DALManager()
        {
            _dbContext = new ContextDotNET2018(); // TODO : éventuellement rajouter le nom d'une DB 
            if (!_dbContext.Database.Exists())  // Vérifier si la DB existe
            {
                Console.WriteLine("Création et remplissage de la DB... !");
                _dbContext.Database.Create();
                DecoderTextToTable dec = new DecoderTextToTable(_dbContext);
                dec.AddMovies(1000);
            }
        }

        #region EtapeDAL
        public IQueryable<Actor> GetActors()
        {
            return _dbContext.Actor;
        }

        public IQueryable<Genre> GetGenres()
        {
            return _dbContext.Genre;
        }

        public IQueryable<Movie> GetMovies()
        {
            return _dbContext.Movie;
        }

        public IQueryable<Comment> GetComments()
        {
            return _dbContext.Comment;
        }

        public void AddComment(String content,int rate,int idMovie,string avatar,DateTime date)
        {
            _dbContext.Comment.Add(new Comment(content,rate,idMovie,avatar,date));
            _dbContext.SaveChanges();
        }
        #endregion

        #region ASPNET
        public IQueryable<Movie> GetMoviesFromXToY(int debut, int combien)
        {
            if (debut == 1) debut = 0;
            var movies = _dbContext.Movie.OrderBy(p => p.Title).Skip(debut).Take(combien);
            
            return movies;
        }

        public IQueryable<Comment> GetCommentsFor(int idMovie)
        {
            return _dbContext.Comment.Where(p => p.IDFilm == idMovie).OrderByDescending(p=>p.Date);
        }
        #endregion
    }
}
