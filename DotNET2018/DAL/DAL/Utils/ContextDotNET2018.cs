﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DAL
{ 
    public class ContextDotNET2018 : DbContext
    {
        #region constructeurs
        public ContextDotNET2018() : base()
        {

        }

        public ContextDotNET2018(string str) : base(str)
        {

        }
        #endregion

        #region proprietes
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Actor> Actor { get; set; }
        public DbSet<Genre> Genre { get; set; }
        public DbSet<Comment> Comment { get; set; }
        #endregion
    }
}

