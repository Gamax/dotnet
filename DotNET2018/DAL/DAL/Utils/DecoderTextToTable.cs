﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DAL
{
    public class DecoderTextToTable
    {
        private List<Actor> ActorsDB;
        private List<Genre> GenresDB;
        private ContextDotNET2018 context;

        public DecoderTextToTable(ContextDotNET2018 cont)
        {
            context = cont;
            ActorsDB = new List<Actor>();
            GenresDB = new List<Genre>();
        }


        public void AddMovies(int lines)
        {
            Movie movie;
            List<String> movies_text = GetMoviesLine(lines);
            foreach (string s in movies_text)
            {
                movie = new Movie();
                try
                {
                    movie = GetMovie(s);
                   
                    context.Movie.Add(movie);
                    Console.WriteLine("Movie Added : " + movie.ID + "|" + movie.Title);
                    context.SaveChanges();
                }
                catch(System.Data.Entity.Infrastructure.DbUpdateException e)
                {
                    Console.WriteLine("Impossible d'ajouter le film !");
                    Console.WriteLine(e.InnerException);
                }
            }
            
        }


        private Movie GetMovie(string str)
        {
            Movie movie = new Movie();

            string[] movieCarac;
            Char[] delimiterChars = {'‣'};
            movieCarac = str.Split(delimiterChars);
            delimiterChars[0] = '\u2016';
            try
            {
                movie.ID = Int32.Parse(movieCarac[0]);
                try
                {
                    movie.Title = movieCarac[1];
                }
                catch (Exception)
                {
                    movie.Title = null;
                }
                try
                {
                    movie.Runtime = Int32.Parse(movieCarac[7]);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Erreur : insertion runtime ");
                    movie.Runtime = -1;
                }
                try
                {
                    movie.PosterPath = movieCarac[9];
                }
                catch (Exception)
                {
                    //Console.WriteLine("Erreur : insertion posterpath ");
                    movie.PosterPath = null; 
                }
                string genres ;
                try
                {
                    genres = movieCarac[12];
                    movie.Genres = GetGenres(genres);
                    foreach(Genre g in movie.Genres)
                    {
                        g.Movies.Add(movie);
                    }
                }
                catch (Exception)
                {
                    //Console.WriteLine("Erreur : insertion genres ");
                }
                string acteurs;
                try
                {
                     acteurs = movieCarac[14];
                    movie.Actors = GetActors(acteurs);
                    foreach (Actor a in movie.Actors)
                    {
                        a.Movies.Add(movie);
                    }
                }
                catch (Exception)
                {
                    //Console.WriteLine("Erreur : insertion acteurs ");
                }
            }
            catch(FormatException)
            {
                return null;
            }
            return movie;
        }

        private List<string> GetMoviesLine(int cpt)
        {
            StreamReader file = new StreamReader(@"movies.txt");
            List<string> moviesToRead = new List<string>() ;

            for(int i = 0; i<cpt; i++)
            {
                moviesToRead.Add(file.ReadLine());
            }

            return moviesToRead;
        }

        private List<Actor> GetActors(string actors)
        {
            List<Actor> Actors = new List<Actor>();
            string[] actorString;
            string[] caracActor;
            Actor act, tempActor;
            Char[] delimiterChars = { '‖' };
            actorString = actors.Split(delimiterChars);
            delimiterChars[0] = '\u2016';

            foreach (string s in actorString)
            {
                try
                {
                    act = new Actor();
                    caracActor = s.Split('․');
                    act.ID = Int32.Parse(caracActor[0]);
                    act.Name = caracActor[1];

                    tempActor = context.Actor.Find(act.ID);
                    if (tempActor != null)
                        act = tempActor;
                    else
                    {
                        foreach (Actor a in Actors)
                        {
                            if (a.ID == act.ID) throw new Exception("Id(" + act.ID + ") déjà inséré !");
                        }

                        foreach(Actor a in ActorsDB)
                        {
                            if (a.ID == act.ID)
                            {
                                act = a;
                                break;
                            }
                        }
                    }
                    
                    ActorsDB.Add(act);
                    Actors.Add(act);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Log : " + e.Message);
                }
            }
            return Actors;
        }

        private List<Genre> GetGenres(string genres)
        {
            List<Genre> Genres = new List<Genre>();
            string[] genreString;
            string[] caracActor;
            Genre gen, tempGenre;
            Char[] delimiterChars = { '‖' };
            genreString = genres.Split(delimiterChars);
            delimiterChars[0] = '\u2016';

            foreach (string s in genreString)
            {
                try
                { 
                    gen = new Genre();
                    caracActor = s.Split('․');
                    gen.ID = Int32.Parse(caracActor[0]);
                    gen.Name = caracActor[1];

                    tempGenre = context.Genre.Find(gen.ID);
                    if (tempGenre != null)
                        gen = tempGenre;
                    else
                    {
                        foreach (Genre g in Genres)
                        {
                            if (g.ID == gen.ID) throw new Exception("Id(" + gen.ID + ") déjà inséré !");
                        }

                        foreach (Genre g in GenresDB)
                        {
                            if (g.ID == gen.ID)
                            {
                                gen = g;
                                break;
                            }
                        }
                    }
                    
                    GenresDB.Add(gen);
                    Genres.Add(gen);
                }
                    catch (Exception e)
                {
                    Console.WriteLine("Error : " + e.Message);
                }
            }
            return Genres;
        }
    }
}
