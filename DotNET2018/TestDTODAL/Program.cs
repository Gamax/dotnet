﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDTODAL;
using DTO;
using BLL;

namespace TestDTODAL
{
    class Program
    {
        static void Main(string[] args)
        {
            BLLManager bLLManager = new BLLManager();

            Console.Out.WriteLine("----- TEST BLL -----");

            Console.Out.WriteLine("----- GetListActorsByMovieId -----");
            String saisir = Console.In.ReadLine();
            Console.Out.WriteLine("----- Results -----");

            List<ActorDTO> actorDTOs = bLLManager.GetListActorsByMovieId(Int32.Parse(saisir));
            foreach(ActorDTO actor in actorDTOs)
            {
                Console.Out.WriteLine(actor.ToString());
            }
            Console.Out.WriteLine("----- EndResults -----");
            
            Console.Out.WriteLine("----- GetMovieTypeListByMovieId -----");
            saisir = Console.In.ReadLine();
            Console.Out.WriteLine("----- Results -----");

            List<MovieTypeDTO> movieTypeDTOs = bLLManager.GetMovieTypeListByMovieId(Int32.Parse(saisir));
            foreach (MovieTypeDTO movieType in movieTypeDTOs)
            {
                Console.Out.WriteLine(movieType.ToString());
            }

            Console.Out.WriteLine("----- EndResults -----");

            Console.Out.WriteLine("----- FindMovieListByPartialActorName -----");
            saisir = Console.In.ReadLine();
            Console.Out.WriteLine("----- Results -----");

            List<MovieDTO> movieDTOs = bLLManager.FindMovieListByPartialActorName(saisir);
            foreach (MovieDTO movie in movieDTOs)
            {
                Console.Out.WriteLine(movie.ToString());
            }

            Console.Out.WriteLine("----- EndResults -----");

            Console.Out.WriteLine("----- GetFavoriteActors -----");
            Console.Out.WriteLine("----- Results -----");

            List<LightActorDTO> lightActorDTOs = bLLManager.GetFavoriteActors();
            foreach (LightActorDTO lightActor in lightActorDTOs)
            {
                Console.Out.WriteLine(lightActor.ToString());
            }

            Console.Out.WriteLine("----- EndResults -----");

            Console.Out.WriteLine("----- GetMovieFullDetailsByMovieId -----");
            saisir = Console.In.ReadLine();
            Console.Out.WriteLine("----- Results -----");

            MovieFullDTO movieFullDTO = bLLManager.GetMovieFullDetailsByMovieId(Int32.Parse(saisir));
            Console.Out.Write(movieFullDTO.ToString());
            Console.Out.WriteLine("----- EndResults -----");

            Console.Out.WriteLine("----- InsertCommentOnMovieId -----");
            Console.Out.Write("Content : ");
            String content = Console.In.ReadLine();
            Console.Out.Write("Rate : ");
            int rate = Int32.Parse(Console.In.ReadLine());
            Console.Out.Write("Avatar : ");
            String avatar = Console.In.ReadLine();
            Console.Out.Write("IDFilm : ");
            int IdFilm = Int32.Parse(Console.In.ReadLine());

            try
            {
                CommentDTO commentDTO = new CommentDTO(content, rate, IdFilm, avatar);
                Console.Out.WriteLine("----- SendingComment -----");

                bLLManager.InsertCommentOnMovieId(commentDTO);

                Console.Out.WriteLine("----- Comment Sent ! -----");
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
                Console.ReadKey();
                return;
               
            }
            
            Console.ReadKey();


        }
    }
}
