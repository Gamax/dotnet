﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using WPFMVVM.Properties;

namespace WPFMVVM
{
    public static class ImageUtilities
    {
        public static BitmapSource getImageFromPosterPath(String posterpath)
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("http://image.tmdb.org/t/p/w185" + posterpath);
            Bitmap bitmapImage = new Bitmap(stream);


            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                     bitmapImage.GetHbitmap(),
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());
        }

        public static BitmapSource getImageFromGenre(int idGenre)
        {
            string path;

            switch (idGenre)
            {
                case 12:
                    path = "adventure";
                    break;
                case 14:
                    path = "fantasy";
                    break;
                case 16:
                    path = "animation";
                    break;
                case 18:
                    path = "drama";
                    break;
                case 27:
                    path = "horror";
                    break;
                case 28:
                    path = "action";
                    break;
                case 35:
                    path = "comedy";
                    break;
                case 36:
                    path = "history";
                    break;
                case 37:
                    path = "western";
                    break;
                case 53:
                    path = "thriller";
                    break;
                case 80:
                    path = "crime";
                    break;
                case 99:
                    path = "documentary";
                    break;
                case 878:
                    path = "sci_fi";
                    break;
                case 9648:
                    path = "mystery";
                    break;
                case 10402:
                    path = "music";
                    break;
                case 10749:
                    path = "romance";
                    break;
                case 10751:
                    path = "family";
                    break;
                case 10752:
                    path = "war";
                    break;
                case 10770:
                    path = "tv";
                    break;
                default:
                    path = "documentary";
                    break;
            }

            return getImageFromPath(path);
        }

        private static BitmapSource getImageFromPath(string path)
        {
            ResourceManager rm = Resources.ResourceManager;
            Bitmap bitmapImage = (Bitmap)rm.GetObject(path);

            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                     bitmapImage.GetHbitmap(),
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());
        }

        public static List<BitmapSource> getRatesImages(double rate)
        {
            List<BitmapSource> listImages = new List<BitmapSource>();

            for (int i = 0; i < 5; i++)
            {
                if (rate >= 1)
                {
                    listImages.Add(getImageFromPath("StarFull"));
                }
                else
                {
                    if (rate >= 0.5)
                    {
                        listImages.Add(getImageFromPath("StarHalf"));
                    }
                    else
                    {
                        listImages.Add(getImageFromPath("StarEmpty"));
                    }
                    
                }
                rate--;

            }

            return listImages;
        }

    }
}
