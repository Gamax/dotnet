﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFMVVM
{
    public class PreviousCommand : ICommand
    {
        private MainWindowViewModel mainWindowViewModel;

        public PreviousCommand(MainWindowViewModel mainWindowViewModel)
        {
            this.mainWindowViewModel = mainWindowViewModel;
        }

        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, new EventArgs());
        }

        public bool CanExecute(object parameter)
        {
                if (mainWindowViewModel.CurrentID < 5)
                return false;
            return true;
        }

        public void Execute(object parameter)
        {

            int indice = mainWindowViewModel.CurrentID - 5;

            mainWindowViewModel.RefreshMovies(indice);
        }
    }
}
