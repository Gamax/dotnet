﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WPFMVVM
{
    public class DetailCommand : ICommand
    {
        private MovieModel movieModel;

        public DetailCommand(MovieModel movieViewModel)
        {
            this.movieModel = movieViewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MovieDetailViewModel movieDetailViewModel = new MovieDetailViewModel(movieModel.ID);
            MovieDetail movieDetail = new MovieDetail(movieDetailViewModel);
            movieDetail.ShowDialog();
        }
    }
}
