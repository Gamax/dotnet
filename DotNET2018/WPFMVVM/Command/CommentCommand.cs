﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFMVVM
{
    public class CommentCommand : ICommand

        
    {
        private MovieDetailViewModel movieDetailViewModel;

        public CommentCommand(MovieDetailViewModel movieDetailViewModel)
        {
            this.movieDetailViewModel = movieDetailViewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            movieDetailViewModel.AddComment();
        }
    }
}
