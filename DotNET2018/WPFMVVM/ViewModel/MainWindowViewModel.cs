﻿using DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFMVVM
{
    public class MainWindowViewModel

    {

        private ObservableCollection<MovieModel> movieModels = new ObservableCollection<MovieModel>();
        private int currentID = 0;
        private NextCommand nextCommand;
        private PreviousCommand previousCommand;


        #region properties
        public ObservableCollection<MovieModel> Movies
        {
            get { return movieModels; }

        }

        public int CurrentID
        {
            get { return currentID; }
        }
        #endregion

        #region constructor
        public MainWindowViewModel()
        {
            nextCommand = new NextCommand(this);
            previousCommand = new PreviousCommand(this);
            RefreshMovies(0);
        }
        #endregion

        #region command
        public ICommand NextClick
        {
            get
            {
                return nextCommand;
            }
        }

        public ICommand PreviousClick
        {
            get
            {
                return previousCommand;
            }
        }



        #endregion

        public void RefreshMovies(int start)
        {
            movieModels.Clear();

            var host = new ServiceRef.WCFServiceClient();
            host.Open();

            MovieDTO[] movieDTOs = host.getMoviesFromXToY(start, 5);
            MovieTypeDTO[] movieTypeDTOs;

            foreach(MovieDTO elem in movieDTOs)
            {
                movieTypeDTOs = host.GetMovieTypeListByMovieId(elem.ID); //recup list genre
                movieModels.Add(new MovieModel(elem, movieTypeDTOs));
            }

            currentID = start;

            previousCommand.RaiseCanExecuteChanged();

        }
    }
}
