﻿using DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace WPFMVVM
{
    public class MovieDetailViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<CommentMovieModel> listComments = new ObservableCollection<CommentMovieModel>();
        private FullMovieModel movieModel;
        private CommentMovieModel editComment = new CommentMovieModel();
        private int idMovie;
        private CommentCommand commentCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        #region properties
        public FullMovieModel Movie
        {
            get { return movieModel; }
        }

        public ObservableCollection<CommentMovieModel> Comments
        {
            get { return listComments; }
        }

        public List<BitmapSource> RateAverageImages
        {
            get { return ImageUtilities.getRatesImages(getVoteAverage()); }
        }

        public double RateAverage
        {
            get { return getVoteAverage(); }
        }


        #endregion

        #region properties Movie
        public BitmapSource Poster
        {
            get { return Movie.Poster; }
        }
        public string Title
        {
            get { return Movie.Title; }
        }
        public int Duration
        {
            get { return Movie.Duration; }
        }
        public List<String> Genres
        {
            get { return Movie.Genres; }
        }
        public List<String> Actors
        {
            get { return Movie.Actors; }
        }

        #endregion

        #region properties comment

        public String CommentRating
        {
            get { return editComment.Rate.ToString(); }
            set
            {
                int temp= Int32.Parse(value);
                if (temp > 5)
                    temp = 5;
                if (temp < 0)
                    temp = 0;
                editComment.Rate = temp;
            }
        }

        public String CommentAvatar
        {
            get { return editComment.Avatar; }
            set { editComment.Avatar = value; }
        }

        public String CommentContent
        {
            get { return editComment.Content; }
            set { editComment.Content = value; }
        }

        public ICommand OnSave
        {
            get { return commentCommand; }
        }

        #endregion

        public MovieDetailViewModel(int idMovie)
        {

            this.idMovie = idMovie;
            
            //récupération infos du film
            var host = new ServiceRef.WCFServiceClient();
            host.Open();

            MovieFullDTO movieDTO = host.GetMovieFullDetailsByMovieId(idMovie);
            movieModel = new FullMovieModel(movieDTO);

            commentCommand = new CommentCommand(this);

            refreshComments();

        }

        public void AddComment()
        {

            if (editComment.Content == "" || editComment.Avatar == "")
                return;

            var host = new ServiceRef.WCFServiceClient();
            host.Open();

            host.InsertCommentOnMovieId(new CommentDTO(editComment.Content, editComment.Rate, idMovie, editComment.Avatar));

            editComment = new CommentMovieModel();

            if(PropertyChanged != null) //on notifie l'ui qu'on a changé les champs
            {
                PropertyChanged(this, new PropertyChangedEventArgs("CommentRating"));
                PropertyChanged(this, new PropertyChangedEventArgs("CommentAvatar"));
                PropertyChanged(this, new PropertyChangedEventArgs("CommentContent"));
                PropertyChanged(this, new PropertyChangedEventArgs("RateAverageImages"));
                PropertyChanged(this, new PropertyChangedEventArgs("RateAverage"));
            }

            refreshComments(); //refresh pour afficher le nouveau commentaire

        }

        public void refreshComments()
        {
            var host = new ServiceRef.WCFServiceClient();
            host.Open();

            listComments.Clear();

            CommentDTO[] commentDTOs = host.getCommentsFor(idMovie);

            foreach(CommentDTO elem in commentDTOs)
            {
                listComments.Add(new CommentMovieModel(elem));
            }
        }

        private double getVoteAverage()
        {
            int numComments = listComments.Count;

            int sum = 0;

            foreach(CommentMovieModel elem in listComments)
            {
                sum += elem.Rate;
            }

            return (double) sum / (double) numComments;
        }
    }
}
