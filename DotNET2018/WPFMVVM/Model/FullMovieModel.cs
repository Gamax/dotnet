﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WPFMVVM
{
    public class FullMovieModel
    {
        private MovieFullDTO movieFullDTO;

        #region properties

        public BitmapSource Poster
        {
            get { return ImageUtilities.getImageFromPosterPath(movieFullDTO.PosterPath); }
        }

        public String Title
        {
            get { return movieFullDTO.Title; }

        }

        public int Duration
        {
            get { return movieFullDTO.Runtime; }
        }

        public List<string> Genres
        {
            get
            {
                List<string> genres = new List<string>();
                foreach(MovieTypeDTO elem in movieFullDTO.MovieTypes)
                {
                    genres.Add(elem.Name);
                }
                return genres;
            }
        }

        public List<string> Actors
        {
            get
            {
                List<string> actors = new List<string>();
                foreach (ActorDTO elem in movieFullDTO.Actors)
                {
                    actors.Add(elem.Name);
                }
                return actors;
            }
        }

        #endregion

        public FullMovieModel(MovieFullDTO movieFullDTO)
        {
            this.movieFullDTO = movieFullDTO;
        }
    }
}
