﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WPFMVVM
{
    public class CommentMovieModel
    {
        private CommentDTO commentDTO;

        public String Content
        {
            get { return commentDTO.Content; }
            set { commentDTO.Content = value; }
        }

        public String Avatar
        {
            get { return commentDTO.Avatar; }
            set { commentDTO.Avatar = value; }
        }

        public int Rate
        {
            get { return commentDTO.Rate; }
            set { commentDTO.Rate = value; }
        }

        public List<BitmapSource> RateImages
        {
            get { return ImageUtilities.getRatesImages(Rate); }
        }

        public String Date
        {
            get { return commentDTO.Date.Hour +":"+commentDTO.Date.Minute+" " + commentDTO.Date.Day + "/" + commentDTO.Date.Month + "/" + commentDTO.Date.Year; }
        }

        public CommentMovieModel()
        {
            commentDTO = new CommentDTO();
            Content = "";
            Avatar = "";
            Rate = 0;
            commentDTO.Date = DateTime.Now;
        }

        public CommentMovieModel(CommentDTO commentDTO)
        {
            this.commentDTO = commentDTO;
        }
    }
}
