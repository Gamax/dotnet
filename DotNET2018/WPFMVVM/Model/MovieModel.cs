﻿using DTO;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace WPFMVVM
{
    public class MovieModel
    {
        private MovieDTO movieDTO;
        private MovieTypeDTO[] movieTypeDTOs;
        private DetailCommand detailCommand;

        #region properties

        public BitmapSource Poster
        {
            get
            {
                return ImageUtilities.getImageFromPosterPath(movieDTO.PosterPath);
                
            }
        }

        public String Title
        {
            get {return movieDTO.Title; }

        }

        public int ID
        {
            get { return movieDTO.ID; }
        }

        public int Duration
        {
            get {return movieDTO.Runtime; }
        }

        public String Genres
        {
            get
            {

                String s = "";
                foreach (MovieTypeDTO elem in movieTypeDTOs)
                {
                    s = s + elem.Name +" ";
                }
                return s;
            }
        }

        public List<BitmapSource> GenresImage
        {
            get
            {
                List<BitmapSource> listGenresImages = new List<BitmapSource>();

                foreach(MovieTypeDTO genre in movieTypeDTOs)
                {
                    listGenresImages.Add(ImageUtilities.getImageFromGenre(genre.ID));
                }

                return listGenresImages;
            }
        }


        #endregion

        public MovieModel(MovieDTO movieDTO, MovieTypeDTO[] movieTypeDTOs)
        {
            this.movieDTO = movieDTO;
            this.movieTypeDTOs = movieTypeDTOs;
            detailCommand = new DetailCommand(this);
        }

        public ICommand DetailClick
        {
            get
            {
                return detailCommand;
            }
        }







    }
}
