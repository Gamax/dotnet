﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DTO;
using BLL;

namespace WCF
{
    [ServiceContract]
    public interface IWCFService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // BLL 
        [OperationContract]
        List<ActorDTO> GetListActorsByIdFilm(int idMovie);

        [OperationContract]
        List<MovieTypeDTO> GetMovieTypeListByMovieId(int idMovie);

        [OperationContract]
        List<MovieDTO> FindMovieListByPartialActorName(String actorName);

        [OperationContract]
        List<LightActorDTO> GetFavoriteActors();

        [OperationContract]
        MovieFullDTO GetMovieFullDetailsByMovieId(int idMovie);

        [OperationContract]
        List<MovieDTO> getMoviesFromXToY(int debut, int combien);

        [OperationContract]
        List<CommentDTO> getCommentsFor(int idMovie);

        [OperationContract]
        double getAverageFor(int idMovie);

        [OperationContract]
        void InsertCommentOnMovieId(CommentDTO comment);


    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "WCF.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
