﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DTO;
using BLL;

namespace WCF
{
    public class WCFService : IWCFService
    {
        private BLLManager bllManager; 

        public WCFService()
        {
            bllManager = new BLLManager();
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }



        public List<ActorDTO> GetListActorsByIdFilm(int idMovie)
        {
            return bllManager.GetListActorsByMovieId(idMovie); 
        }
        public List<MovieTypeDTO> GetMovieTypeListByMovieId(int idMovie)
        {
            return bllManager.GetMovieTypeListByMovieId(idMovie);
        }
        public List<MovieDTO> FindMovieListByPartialActorName(String actorName)
        {
            return bllManager.FindMovieListByPartialActorName(actorName);
        }
        public List<LightActorDTO> GetFavoriteActors()
        {
            return bllManager.GetFavoriteActors();
        }
        public MovieFullDTO GetMovieFullDetailsByMovieId(int idMovie)
        {
            return bllManager.GetMovieFullDetailsByMovieId(idMovie);
        }
        public void InsertCommentOnMovieId(CommentDTO comment)
        {
            bllManager.InsertCommentOnMovieId(comment);
        }

        public List<MovieDTO> getMoviesFromXToY(int debut, int combien)
        {
            return bllManager.getMoviesFromXToY(debut,combien);
        }

        public List<CommentDTO> getCommentsFor(int idMovie)
        {
            return bllManager.getCommentsFor(idMovie);
        }

        public double getAverageFor(int idMovie)
        {
            return bllManager.getAverageFor(idMovie);
        }
    }
}
