﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DotNET2018
{
    class TestDAL
    {
        static void Main(string[] args)
        {
            int nb_Films, nb_Acteurs;
            ContextDotNET2018 context = new ContextDotNET2018();

            if (!context.Database.Exists())  // Vérifier si la DB existe
            {
                Console.WriteLine("Création et remplissage de la DB... !");
                context.Database.Create();
                DecoderTextToTable dec = new DecoderTextToTable(context);
                dec.AddMovies(1000);
            }
            else
                Console.WriteLine("DB existe déjà... !");

            Console.WriteLine("La DB contient : ");

            nb_Films = context.Movie.Count();
            nb_Acteurs = context.Actor.Count();

            Console.WriteLine(nb_Films + " films");
            Console.WriteLine(nb_Acteurs + " acteurs");
            

            IQueryable<Movie> listes = context.Movie.Take(5);

            foreach(Movie m in listes)
            {
                m.Affiche();
                Console.WriteLine(" ");
                Console.ReadKey();
            }

            System.Environment.Exit(0);
        }
    }
}
