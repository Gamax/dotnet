﻿
using System;

namespace WebFilms.Models
{
    public class CommentModel
    {
        private string _content;
        private double _rate;
        private int _idfilm;
        private string _avatar;
        private DateTime _date;
        private string _Star1;
        private string _Star2;
        private string _Star3;
        private string _Star4;
        private string _Star5;

        #region properties
        public string Content { get => _content; set => _content = value; }
        public int Idfilm { get => _idfilm; set => _idfilm = value; }
        public string Avatar { get => _avatar; set => _avatar = value; }
        public string Star1 { get => _Star1; }
        public string Star2 { get => _Star2; }
        public string Star3 { get => _Star3; }
        public string Star4 { get => _Star4; }
        public string Star5 { get => _Star5; }
        public double Rate
        {
            get { return _rate; }
            set
            {
                if (value > 5)
                {
                    _rate = 5;
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                if (value < 0)
                {
                    _rate = 0 ; 
                    _Star1 = "/Resources/StarEmpty.gif";
                    _Star2 = "/Resources/StarEmpty.gif";
                    _Star3 = "/Resources/StarEmpty.gif";
                    _Star4 = "/Resources/StarEmpty.gif";
                    _Star5 = "/Resources/StarEmpty.gif";

                }
                _rate = value;
                if (value == 5)
                {
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                else
                {
                    if (value > 4)
                    {
                        _Star1 = "/Resources/StarFull.gif";
                        _Star2 = "/Resources/StarFull.gif";
                        _Star3 = "/Resources/StarFull.gif";
                        _Star4 = "/Resources/StarFull.gif";
                        _Star5 = "/Resources/StarHalf.gif";
                    }
                    else
                    {
                        if (value == 4)
                        {
                            _Star1 = "/Resources/StarFull.gif";
                            _Star2 = "/Resources/StarFull.gif";
                            _Star3 = "/Resources/StarFull.gif";
                            _Star4 = "/Resources/StarFull.gif";
                            _Star5 = "/Resources/StarEmpty.gif";
                        }
                        else
                        {
                            if (value > 3)
                            {
                                _Star1 = "/Resources/StarFull.gif";
                                _Star2 = "/Resources/StarFull.gif";
                                _Star3 = "/Resources/StarFull.gif";
                                _Star4 = "/Resources/StarHalf.gif";
                                _Star5 = "/Resources/StarEmpty.gif";
                            }
                            else
                            {
                                if (value == 3)
                                {
                                    _Star1 = "/Resources/StarFull.gif";
                                    _Star2 = "/Resources/StarFull.gif";
                                    _Star3 = "/Resources/StarFull.gif";
                                    _Star4 = "/Resources/StarEmpty.gif";
                                    _Star5 = "/Resources/StarEmpty.gif";
                                }
                                else
                                {
                                    if (value > 2)
                                    {
                                        _Star1 = "/Resources/StarFull.gif";
                                        _Star2 = "/Resources/StarFull.gif";
                                        _Star3 = "/Resources/StarHalf.gif";
                                        _Star4 = "/Resources/StarEmpty.gif";
                                        _Star5 = "/Resources/StarEmpty.gif";
                                    }
                                    else
                                    {
                                        if (value == 2)
                                        {
                                            _Star1 = "/Resources/StarFull.gif";
                                            _Star2 = "/Resources/StarFull.gif";
                                            _Star3 = "/Resources/StarEmpty.gif";
                                            _Star4 = "/Resources/StarEmpty.gif";
                                            _Star5 = "/Resources/StarEmpty.gif";
                                        }
                                        else
                                        {
                                            if (value > 1)
                                            {
                                                _Star1 = "/Resources/StarFull.gif";
                                                _Star2 = "/Resources/StarHalf.gif";
                                                _Star3 = "/Resources/StarEmpty.gif";
                                                _Star4 = "/Resources/StarEmpty.gif";
                                                _Star5 = "/Resources/StarEmpty.gif";
                                            }
                                            else
                                            {
                                                if (value == 1)
                                                {
                                                    _Star1 = "/Resources/StarFull.gif";
                                                    _Star2 = "/Resources/StarEmpty.gif";
                                                    _Star3 = "/Resources/StarEmpty.gif";
                                                    _Star4 = "/Resources/StarEmpty.gif";
                                                    _Star5 = "/Resources/StarEmpty.gif";
                                                }
                                                else
                                                {
                                                    if (value > 0)
                                                    {
                                                        _Star1 = "/Resources/StarHalf.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                    else
                                                    {
                                                        _Star1 = "/Resources/StarEmpty.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        
        public DateTime Date { get => _date; set => _date = value; }

        #endregion
        public CommentModel()
        {

        }

        public CommentModel(string content, double rate, int idFilm, string avatar,DateTime date)
        {
            Content = content;
            Rate = rate;
            Idfilm = idFilm;
            Avatar = avatar;
            Date = date;
        }
    }
}