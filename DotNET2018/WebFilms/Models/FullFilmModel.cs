﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTO;

namespace WebFilms.Models
{
    public class FullFilmModel
    {
        #region variables
        int _Id;
        string _PosterPath;
        string _Title;
        int _Runtime;
        double _Rating;
        List<ActorDTO> _Actors;
        List<MovieTypeDTO> _Types;
        private string _Star1;
        private string _Star2;
        private string _Star3;
        private string _Star4;
        private string _Star5;
        #endregion

        #region Properties
        public int Id { get => _Id; set => _Id = value; }
        public string PosterPath { get => _PosterPath; set => _PosterPath = value; }
        public string Title { get => _Title; set => _Title = value; }
        public int Runtime { get => _Runtime; set => _Runtime = value; }
        public string RuntimeString
        {
            get
            {
                if (Runtime < 60)
                    return Runtime.ToString() + " min";

                var result = TimeSpan.FromMinutes(Runtime);
                int hours = (int)result.TotalHours;
                int minutes = (int)result.Minutes;

                if (minutes < 10)
                    return hours.ToString() + "h0" + minutes.ToString();

                return hours.ToString() + "h" + minutes.ToString();
            }
        }
        public double Rating
        {
            get { return _Rating; }
            set
            {
                _Rating = value;
                if (value > 5)
                {
                    _Rating = 5;
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                if (value < 0)
                {
                    _Rating = 0;
                    _Star1 = "/Resources/StarEmpty.gif";
                    _Star2 = "/Resources/StarEmpty.gif";
                    _Star3 = "/Resources/StarEmpty.gif";
                    _Star4 = "/Resources/StarEmpty.gif";
                    _Star5 = "/Resources/StarEmpty.gif";
                }

                if (value == 5)
                {
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                else
                {
                    if (value > 4)
                    {
                        _Star1 = "/Resources/StarFull.gif";
                        _Star2 = "/Resources/StarFull.gif";
                        _Star3 = "/Resources/StarFull.gif";
                        _Star4 = "/Resources/StarFull.gif";
                        _Star5 = "/Resources/StarHalf.gif";
                    }
                    else
                    {
                        if (value == 4)
                        {
                            _Star1 = "/Resources/StarFull.gif";
                            _Star2 = "/Resources/StarFull.gif";
                            _Star3 = "/Resources/StarFull.gif";
                            _Star4 = "/Resources/StarFull.gif";
                            _Star5 = "/Resources/StarEmpty.gif";
                        }
                        else
                        {
                            if (value > 3)
                            {
                                _Star1 = "/Resources/StarFull.gif";
                                _Star2 = "/Resources/StarFull.gif";
                                _Star3 = "/Resources/StarFull.gif";
                                _Star4 = "/Resources/StarHalf.gif";
                                _Star5 = "/Resources/StarEmpty.gif";
                            }
                            else
                            {
                                if (value == 3)
                                {
                                    _Star1 = "/Resources/StarFull.gif";
                                    _Star2 = "/Resources/StarFull.gif";
                                    _Star3 = "/Resources/StarFull.gif";
                                    _Star4 = "/Resources/StarEmpty.gif";
                                    _Star5 = "/Resources/StarEmpty.gif";
                                }
                                else
                                {
                                    if (value > 2)
                                    {
                                        _Star1 = "/Resources/StarFull.gif";
                                        _Star2 = "/Resources/StarFull.gif";
                                        _Star3 = "/Resources/StarHalf.gif";
                                        _Star4 = "/Resources/StarEmpty.gif";
                                        _Star5 = "/Resources/StarEmpty.gif";
                                    }
                                    else
                                    {
                                        if (value == 2)
                                        {
                                            _Star1 = "/Resources/StarFull.gif";
                                            _Star2 = "/Resources/StarFull.gif";
                                            _Star3 = "/Resources/StarEmpty.gif";
                                            _Star4 = "/Resources/StarEmpty.gif";
                                            _Star5 = "/Resources/StarEmpty.gif";
                                        }
                                        else
                                        {
                                            if (value > 1)
                                            {
                                                _Star1 = "/Resources/StarFull.gif";
                                                _Star2 = "/Resources/StarHalf.gif";
                                                _Star3 = "/Resources/StarEmpty.gif";
                                                _Star4 = "/Resources/StarEmpty.gif";
                                                _Star5 = "/Resources/StarEmpty.gif";
                                            }
                                            else
                                            {
                                                if (value == 1)
                                                {
                                                    _Star1 = "/Resources/StarFull.gif";
                                                    _Star2 = "/Resources/StarEmpty.gif";
                                                    _Star3 = "/Resources/StarEmpty.gif";
                                                    _Star4 = "/Resources/StarEmpty.gif";
                                                    _Star5 = "/Resources/StarEmpty.gif";
                                                }
                                                else
                                                {
                                                    if (value > 0)
                                                    {
                                                        _Star1 = "/Resources/StarHalf.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                    else
                                                    {
                                                        _Star1 = "/Resources/StarEmpty.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        public List<ActorDTO> Actors { get => _Actors; set => _Actors = value; }
        public List<MovieTypeDTO> Types { get => _Types; set => _Types = value; }
        public string Star1 { get => _Star1; }
        public string Star2 { get => _Star2; }
        public string Star3 { get => _Star3; }
        public string Star4 { get => _Star4; }
        public string Star5 { get => _Star5; }
        #endregion

        public FullFilmModel()
        {
            Actors = new List<ActorDTO>();
            Types = new List<MovieTypeDTO>();
        }

        public FullFilmModel(int id, string poster, string title, int runtime, double rating, List<ActorDTO> actors, List<MovieTypeDTO> types)
        {
            Id = id;
            PosterPath = poster;
            Title = title;
            Runtime = runtime;
            Rating = rating;
            Actors = actors;
            Types = types;
        }
    }
}