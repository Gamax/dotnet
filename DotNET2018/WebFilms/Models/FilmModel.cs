﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFilms.Models
{
    public class FilmModel
    {
        #region variables
        private int _Id;
        private int _Colonne;
        private string _Titre;
        private int _Runtime;
        private double _Rating;
        private string _Star1;
        private string _Star2;
        private string _Star3;
        private string _Star4;
        private string _Star5;
        #endregion
        
        #region constructeurs
        public FilmModel()
        {

        }

        public FilmModel(int colonne, int id, string titre, int runtime, double rating)
        {
            Colonne = colonne;
            Id = id;
            Titre = titre;
            Runtime = runtime;
            Rating = rating;
        }
        #endregion

        #region properties
        public int Id { get => _Id; set => _Id = value; }
        public string Titre { get => _Titre; set => _Titre = value; }
        public int Runtime { get => _Runtime; set => _Runtime = value; }
        public string RuntimeString {
            get
            {
                if (Runtime < 60)
                    return Runtime.ToString() + " min";

                var result = TimeSpan.FromMinutes(Runtime);
                int hours = (int)result.TotalHours;
                int minutes = (int)result.Minutes;

                if (minutes < 10)
                    return hours.ToString() + "h0" + minutes.ToString();

                return hours.ToString()+"h"+minutes.ToString() ;
            }
        }
        public string Star1 { get => _Star1; }
        public string Star2 { get => _Star2; }
        public string Star3 { get => _Star3; }
        public string Star4 { get => _Star4; }
        public string Star5 { get => _Star5; }
        public double Rating
        { 
            get { return _Rating; }
            set {
                _Rating = value;
                if (value > 5)
                {
                    _Rating = 5;
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                if (value < 0)
                {
                    _Rating = 0;
                    _Star1 = "/Resources/StarEmpty.gif";
                    _Star2 = "/Resources/StarEmpty.gif";
                    _Star3 = "/Resources/StarEmpty.gif";
                    _Star4 = "/Resources/StarEmpty.gif";
                    _Star5 = "/Resources/StarEmpty.gif";
                }
                
                if(value == 5)
                {
                    _Star1 = "/Resources/StarFull.gif";
                    _Star2 = "/Resources/StarFull.gif";
                    _Star3 = "/Resources/StarFull.gif";
                    _Star4 = "/Resources/StarFull.gif";
                    _Star5 = "/Resources/StarFull.gif";
                }
                else
                {
                    if(value > 4)
                    {
                        _Star1 = "/Resources/StarFull.gif";
                        _Star2 = "/Resources/StarFull.gif";
                        _Star3 = "/Resources/StarFull.gif";
                        _Star4 = "/Resources/StarFull.gif";
                        _Star5 = "/Resources/StarHalf.gif";
                    }
                    else
                    {
                        if (value == 4)
                        {
                            _Star1 = "/Resources/StarFull.gif";
                            _Star2 = "/Resources/StarFull.gif";
                            _Star3 = "/Resources/StarFull.gif";
                            _Star4 = "/Resources/StarFull.gif";
                            _Star5 = "/Resources/StarEmpty.gif";
                        }
                        else
                        {
                            if(value > 3)
                            {
                                _Star1 = "/Resources/StarFull.gif";
                                _Star2 = "/Resources/StarFull.gif";
                                _Star3 = "/Resources/StarFull.gif";
                                _Star4 = "/Resources/StarHalf.gif";
                                _Star5 = "/Resources/StarEmpty.gif";
                            }
                            else
                            {
                                if (value == 3)
                                {
                                    _Star1 = "/Resources/StarFull.gif";
                                    _Star2 = "/Resources/StarFull.gif";
                                    _Star3 = "/Resources/StarFull.gif";
                                    _Star4 = "/Resources/StarEmpty.gif";
                                    _Star5 = "/Resources/StarEmpty.gif";
                                }
                                else
                                {
                                    if(value > 2)
                                    {
                                        _Star1 = "/Resources/StarFull.gif";
                                        _Star2 = "/Resources/StarFull.gif";
                                        _Star3 = "/Resources/StarHalf.gif";
                                        _Star4 = "/Resources/StarEmpty.gif";
                                        _Star5 = "/Resources/StarEmpty.gif";
                                    }
                                    else
                                    {
                                        if(value == 2)
                                        {
                                            _Star1 = "/Resources/StarFull.gif";
                                            _Star2 = "/Resources/StarFull.gif";
                                            _Star3 = "/Resources/StarEmpty.gif";
                                            _Star4 = "/Resources/StarEmpty.gif";
                                            _Star5 = "/Resources/StarEmpty.gif";
                                        }
                                        else
                                        {
                                            if(value > 1)
                                            {
                                                _Star1 = "/Resources/StarFull.gif";
                                                _Star2 = "/Resources/StarHalf.gif";
                                                _Star3 = "/Resources/StarEmpty.gif";
                                                _Star4 = "/Resources/StarEmpty.gif";
                                                _Star5 = "/Resources/StarEmpty.gif";
                                            }
                                            else
                                            {
                                                if (value == 1)
                                                {
                                                    _Star1 = "/Resources/StarFull.gif";
                                                    _Star2 = "/Resources/StarEmpty.gif";
                                                    _Star3 = "/Resources/StarEmpty.gif";
                                                    _Star4 = "/Resources/StarEmpty.gif";
                                                    _Star5 = "/Resources/StarEmpty.gif";
                                                }
                                                else
                                                {
                                                    if(value > 0)
                                                    {
                                                        _Star1 = "/Resources/StarHalf.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                    else
                                                    {
                                                        _Star1 = "/Resources/StarEmpty.gif";
                                                        _Star2 = "/Resources/StarEmpty.gif";
                                                        _Star3 = "/Resources/StarEmpty.gif";
                                                        _Star4 = "/Resources/StarEmpty.gif";
                                                        _Star5 = "/Resources/StarEmpty.gif";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        public int Colonne { get => _Colonne; set => _Colonne = value; }
        #endregion
    }
}