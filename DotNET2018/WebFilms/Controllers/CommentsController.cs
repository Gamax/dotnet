﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFilms.Models;

namespace WebFilms.Controllers
{
    public class CommentsController : Controller
    {
        // GET: Comments
        public ActionResult Comments(int idMovie)
        {
            wcfWeb.WCFServiceClient host = new wcfWeb.WCFServiceClient();
            host.Open();

            CommentDTO[] comments = host.getCommentsFor(idMovie);
            List<CommentModel> Comments = new List<CommentModel>();
            foreach(CommentDTO comment in comments)
            {
                Comments.Add(new CommentModel(comment.Content,comment.Rate,comment.IDFilm,comment.Avatar,comment.Date));
            }
            if (Comments.Count < 1)
                Comments.Add(new CommentModel("CHECKUP_ADMIN", 0, idMovie, "",DateTime.Today));

            return View(Comments);
        }

        public ActionResult New(int idMovie)
        {
            CommentModel comment = new CommentModel();
            comment.Idfilm = idMovie;
            return View(comment);
        }

        public ActionResult Cancel(int idFilm)
        {
            return RedirectToAction("Comments", new { idMovie = idFilm });
        }

        public ActionResult Edit(CommentModel comment)
        {
            wcfWeb.WCFServiceClient host = new wcfWeb.WCFServiceClient();
            host.Open();
            if (comment.Avatar == null )
                return RedirectToAction("New",new { idMovie = comment.Idfilm });
            CommentDTO Comment = new CommentDTO(comment.Content, Convert.ToInt32(comment.Rate),comment.Idfilm, comment.Avatar);
            if (Comment.Content == null)
                Comment.Content = "";
            host.InsertCommentOnMovieId(Comment);

            return RedirectToAction("Comments", new { idMovie = comment.Idfilm });
        }
    }
}