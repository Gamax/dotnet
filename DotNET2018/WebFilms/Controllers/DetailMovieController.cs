﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFilms.Models;

namespace WebFilms.Controllers
{
    public class DetailMovieController : Controller
    {
        // GET: DetailMovie
        public ActionResult Detail(int id)
        {
            FullFilmModel film = getMovie(id);

            return View(film);
        }

        private FullFilmModel getMovie(int id)
        {
            try
            {
                wcfWeb.WCFServiceClient host = new wcfWeb.WCFServiceClient();
                host.Open();

                MovieFullDTO movieFullDTO = host.GetMovieFullDetailsByMovieId(id);
                double rate = host.getAverageFor(id);
                FullFilmModel movieFull = new FullFilmModel(movieFullDTO.ID, "http://image.tmdb.org/t/p/w185" + movieFullDTO.PosterPath, movieFullDTO.Title, movieFullDTO.Runtime, rate, movieFullDTO.Actors, movieFullDTO.MovieTypes);
                return movieFull;
            }
            catch (Exception e)
            {

            }

            return new FullFilmModel();
        }
    }
}