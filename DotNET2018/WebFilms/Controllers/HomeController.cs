﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebFilms.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Programmation .NET laboratoire 2018 - 1019";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "About us :";

            return View();
        }

    }
}