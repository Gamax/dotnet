﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFilms.Models;

namespace WebFilms.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie

        public ActionResult Movies()
        {
            var listeFilms = getMovies(0);
            return View(listeFilms);
        }

        public ActionResult Precedent(int debut)
        {
            var listeFilms = getMovies(debut - 10);
            return View("Movies", listeFilms);
        }

        public ActionResult Next(int debut)
        {
            var listeFilms = getMovies(debut + 10);
            return View("Movies", listeFilms);
        }

        private List<FilmModel> getMovies(int debut)
        {
            try
            {
                wcfWeb.WCFServiceClient host = new wcfWeb.WCFServiceClient();
                host.Open();

                MovieDTO[] moviesDTO = host.getMoviesFromXToY(debut,10);
                List<FilmModel> films = new List<FilmModel>();
                double rating = 0;
                foreach(MovieDTO movie in moviesDTO)
                {
                    rating = host.getAverageFor(movie.ID);
                    films.Add(new FilmModel(debut, movie.ID, movie.Title, movie.Runtime, rating));
                    rating = 0;
                }
                if(films.Count < 1)
                {
                    films.Add(new FilmModel(debut, 0, "No film", 0, 0));
                }
                return films;
            }
            catch(Exception e)
            {

            }
            
            return new List<FilmModel>();
        }

    }
}