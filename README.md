## Notes

### BLL

Il faut une méthode 

```c#
List<LightADTO> GetFavoriteActors(int n) {
    var query = DALManager.GetNFavoriteActor(n);
    var list = (from a in query
        		select new LightADTO()
                { = ...
                    
                }
     ).ToList();
    return list;
}
```

### DAL

```c#
IQueryable<Actor> GetNFavoriteActor(int n)
{
    {var query = from a in XXDbContext.Actors
        		where a.Films.count >= n
        		select a
    }
    return query;
}
```

